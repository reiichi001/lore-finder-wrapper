<form action="/lorefinder.php" method="get">
<input type="text" name="string" size=30>
<button >Search the lore finder</button>
<br />
<!-- <input type="checkbox" name="loadnames" value="1"> Load names, pictures, and generate links (this will increase page load) -->
</form>

<?php
$redis = new Redis();
$redis->connect('127.0.0.1', 6379);

// we need these to query

$api_key = "APIKEYHERE";
$url = "https://xivapi.com/lore";
$renderthings = false;
$query = "";
$curlme = $url;
$searchstring = "";
$pagenum = "1";

if (isset($_REQUEST["string"]))
{
    $searchstring = $_REQUEST["string"];
    $curlme.= "?string=" . $_REQUEST["string"];
    $renderthings = true;
    $query = $_REQUEST["string"];
}

if (isset($_REQUEST["page"]))
{
    $curlme.= "&page=" . $_REQUEST["page"];
    $pagenum = $_REQUEST["page"];
}

$curlme.= "&columns=Source,SourceID,Context,Text,Data&limit=50";
$curlme.= "&key=" . $api_key;

if ($renderthings)
{

    // echo $curlme . "<br />";

    /*
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $curlme );
    $result = curl_exec($ch);
    curl_close($ch);
    */
    $cachethissearch = $searchstring . "p" . $pagenum . "o50";
    $result = null;
    if ($redis->exists($cachethissearch))
    {

        // echo "Redis found cached results! <br />";

        $result = $redis->get($cachethissearch);
    }
    else
    {

        // echo "Caching these results in Redis so we don't overload the API if this is searched again soon. <br />";

        $result = file_get_contents($curlme);
        $redis->set($cachethissearch, $result);
    }

    $json_object = json_decode($result, true);
    $pagination_object = $json_object["Pagination"];
    $results_array = $json_object["Results"];
    if (count($results_array) > 0)
    {
        add_pagination($query, $pagination_object);
        foreach($results_array as $entry)
        {
            $entrycontext = $entry["Context"];
            $entrysource = $entry["Source"];
            $entrysourceid = $entry["SourceID"];
            $entrytext = $entry["Text"];
            $entrydata = $entry["Data"];
            echo "<p>";

            // generategarlondlink($entrysource,$entrysourceid, $redis);

            if ($entrydata != null)
            {
                $name = $entrydata["Name"];
                $icon = $entrydata["Icon"];
                if ($entrysource === "Fate")
                {
                    echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
                    echo "<a href='https://www.garlandtools.org/db/#fate/" . $entrysourceid . "' target='_blank'>" . $name . "</a>" . "<br />";
                }
                else
                if ($entrysource === "Item")
                {
                    echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
                    echo $item_name = "<a href='https://www.garlandtools.org/db/#item/" . $entrysourceid . "' target='_blank'>" . $name . "</a>" . "<br />";
                }
                else
                if ($entrysource === "Leve")
                {
                    echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
                    echo $leve_name = "<a href='https://www.garlandtools.org/db/#leve/" . $entrysourceid . "' target='_blank'>" . $name . "</a>" . "<br />";
                }
                else
                if ($entrysource === "Mount")
                {
                    echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
                    echo $mount_name = "<a href='" . $curlme . "&pretty=1" . "' target='_blank'>" . $name . "</a>" . "<br />";
                }
                else
                if ($entrysource === "Quest")
                {
                    echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
                    echo $quest_name = "<a href='https://www.garlandtools.org/db/#quest/" . $entrysourceid . "' target='_blank'>" . $name . "</a>" . "<br />";
                }
            }

            echo "Context: " . $entrycontext . "<br />";
            echo "Source: " . $entrysource . "<br />";
            echo "SourceID: " . $entrysourceid . "<br />";
            echo "Text: " . $entrytext . "<br />";
            echo "</p>";
        }

        add_pagination($query, $pagination_object);
    }
    else echo "There were no results." . "<br />";

    // header('Content-Type: application/json');
    // echo nl2br(str_replace(' ', '&nbsp;', (json_encode($results_array, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES))));
    // echo json_encode(json_decode($result), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    // echo print

}

function add_pagination($query, $pgo)
{

    // Check if we have multiple pages of JSON to loop over

    $currentpage = $pgo["Page"];
    $nextpage = $pgo["PageNext"];
    $previouspage = $pgo["PagePrev"];
    $totalpages = $pgo["PageTotal"];
    $resultsnum = $pgo["ResultsTotal"];
    $resultsppnum = $pgo["ResultsPerPage"];
    $addtome = $resultsppnum * ($currentpage - 1) + 1;
    $addtome2 = $resultsppnum * $currentpage;
    if ($addtome2 > $resultsnum) $addtome2 = $resultsnum;
    echo "Showing results " . $addtome . "-" . $addtome2 . " of " . $resultsnum . "<br />";
    if ($totalpages > 1)
    {

        // we're dealing with multiple pages of results here
        // echo "This is page " . $currentpage . " of " . $totalpages . ". <br />";

        $nextpagelink = "<a href='lorefinder.php?string=" . $query . "&page=" . $nextpage;
        $previouspagelink = "<a href='lorefinder.php?string=" . $query . "&page=" . $previouspage;
        /*
        if (isset($_REQUEST['loadnames']) && $_REQUEST['loadnames'] == 1)
        {
        $nextpagelink .= "&loadnames=1";
        $previouspagelink .= "&loadnames=1";
        }

        */
        $nextpagelink.= "'>Next Page</a>";
        $previouspagelink.= "'>Previous Page</a>";
        if ($previouspage != null && $previouspage < $currentpage) echo $previouspagelink . "<br />";
        if ($nextpage != null && $nextpage > $currentpage) echo $nextpagelink . "<br />";
    }
}

function generategarlondlink($source, $sourceid, $redis)
{
    if ($source === "Fate")
    {
        echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
        echo "<a href='https://www.garlandtools.org/db/#fate/" . $sourceid . "' target='_blank'>" . $name . "</a>" . "<br />";
    }
    else
    if ($source === "Item")
    {
        echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
        echo $item_name = "<a href='https://www.garlandtools.org/db/#item/" . $sourceid . "' target='_blank'>" . $name . "</a>" . "<
br />";
    }
    else
    if ($source === "Leve")
    {
        echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
        echo $leve_name = "<a href='https://www.garlandtools.org/db/#leve/" . $sourceid . "' target='_blank'>" . $name . "</a>" . "<
br />";
    }
    else
    if ($source === "Mount")
    {
        echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
        echo $mount_name = "<a href='" . $curlme . "&pretty=1" . "' target='_blank'>" . $name . "</a>" . "<br />";
    }
    else
    if ($source === "Quest")
    {
        echo "<img src='https://xivapi.com" . $icon . "'>" . "<br />";
        echo $quest_name = "<a href='https://www.garlandtools.org/db/#quest/" . $sourceid . "' target='_blank'>" . $name . "</a>" . "<br />";
    }
}
